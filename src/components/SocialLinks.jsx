import React from "react";
import { FaGithub, FaLinkedin } from "react-icons/fa";
import { HiOutlineMail } from "react-icons/hi";
import { BsFillPersonLinesFill } from "react-icons/bs";

const SocialLinks = () => {
  const socialLinks = [
    {
      id: 1,
      child: (
        <>
          LinkedIn <FaLinkedin size={30} />
        </>
      ),
      href: "https://linkedin.com",
      style: "rounded-tr-md",
    },
    {
      id: 2,
      child: (
        <>
          GitHub <FaGithub size={30} />
        </>
      ),
      href: "https://github.com/",
    },
    {
      id: 3,
      child: (
        <>
          Mail <HiOutlineMail size={30} />
        </>
      ),
      href: "mailto:na@gmail.com",
    },
    {
      id: 4,
      child: (
        <>
          Resume <BsFillPersonLinesFill size={30} />
        </>
      ),
      style: "rounded-br-md",
      href: "https://mail.google.com/",
      download: true,
    },
  ];

  const socialLink = socialLinks.map((socialLinkItem) => (
    <li
      key={socialLinkItem.id}
      className={
        "flex justify-between w-40 h-10 px-4 ml-[-6rem] hover:ml-[-.5rem] hover:rounded-md duration-300 bg-gray-500" +
        " " +
        socialLinkItem.style
      }
    >
      <a
        href={socialLinkItem.href ? socialLinkItem.href : "/"}
        className="flex justify-between items-center w-full text-white"
        download={socialLinkItem.download}
        target="_blank"
        rel="noreferrer"
      >
        {socialLinkItem.child}
      </a>
    </li>
  ));

  return (
    <div className="hidden flex-col top-[35%] left-0 fixed lg:flex">
      <ul>{socialLink}</ul>
    </div>
  );
};

export default SocialLinks;

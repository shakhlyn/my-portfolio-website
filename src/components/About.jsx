const About = () => {
  return (
    <div className="md:h-screen text-gray-300 bg-gradient-to-b from-gray-800  to-gray-900 ">
      <div className=" max-w-screen-lg px-8 flex flex-col justify-center mx-auto w-full h-full ">
        <h2 className="my-12 text-4xl font-bold inline border-b-4 border-gray-500 ">
          About
        </h2>
        <p className=" text-xl ">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias libero
          dignissimos similique vel molestiae temporibus necessitatibus illo ea
          nulla odit! Libero sapiente eos veniam temporibus vel minima similique
          quidem tempora!
        </p>
        <br />
        <p className=" text-xl ">
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Recusandae
          doloremque aliquid vel itaque porro, nostrum molestias cupiditate
          accusamus totam excepturi sit, velit magni minus, quia dignissimos
          dolores fuga accusantium libero? Quam repellat libero consequatur, qui
          nemo reprehenderit accusamus? Exercitationem quae ducimus ea deleniti,
          est tempore totam iure officia! Excepturi soluta repellat enim sunt.
          Ratione numquam distinctio repellendus reprehenderit eligendi
          laboriosam!
        </p>
      </div>
    </div>
  );
};

export default About;

import { FaBars, FaTimes } from "react-icons/fa";
import { useState } from "react";

const NavBar = () => {
  const [navBarIsShown, setNavBarIsShown] = useState(false);

  const navItems = [
    { id: "home", link: "home" },
    { id: "about", link: "about" },
    { id: "skills", link: "skills" },
    { id: "experience", link: "experience" },
    { id: "contact", link: "contact" },
  ];

  const navLinks = navItems.map((navItem) => {
    return (
      <li
        key={navItem.id}
        className="px-4 py-1 bg-gray-900 text-xl text-gray-400 cursor-pointer item-center capitalize hover:scale-105 duration-200 "
      >
        {navItem.link}
      </li>
    );
  });

  const hangleToggleNavBar = () => {
    setNavBarIsShown(!navBarIsShown);
  };

  return (
    <div className=" bg-gray-900 py-5 flex justify-between items-center min-w-full px-4 fixed h-20">
      <div>
        <h1 className="text-5xl text-gray-400 font-signature font-bold ml-2">
          Shakhlyn
        </h1>
      </div>
      <div>
        <ul className="hidden md:flex">{navLinks}</ul>
      </div>
      <div
        onClick={hangleToggleNavBar}
        className="pr-4 text-gray-400 items-center cursor-pointer md:hidden z-10 "
      >
        {navBarIsShown ? <FaTimes /> : <FaBars />}
      </div>
      <div className="md:hidden list-none absolute right-0 top-16 ">
        {navBarIsShown && <ul>{navLinks}</ul>}
      </div>
    </div>
  );
};

export default NavBar;

// import React, { useState } from "react";
// import { FaBars, FaTimes } from "react-icons/fa";
// import NavLinks from "./NavLinks";

// const NavBar = () => {
//   const [nav, setNav] = useState(false);

//   const links = [
//     { id: 1, link: "home" },
//     { id: 2, link: "about" },
//     { id: 3, link: "portfolio" },
//     { id: 4, link: "experience" },
//     { id: 5, link: "contact" },
//   ];

//   const toggleNav = () => {
//     setNav(!nav);
//   };

//   const navLinkClass =
//     "px-4 cursor-pointer capitalize font-medium text-gray-500 hover:scale-105 duration-200";

//   const renderNavLinks = () => {
//     return links.map(({ id, link }) => (
//       <NavLinks
//         key={id}
//         link={link}
//         className={navLinkClass}
//       />
//     ));
//   };

//   return (
//     <div className="flex justify-between items-center w-full h-20 px-4 bg-black text-white fixed">
//       <div>
//         <h1 className="text-5xl font-signature ml-2">Yash</h1>
//       </div>

//       <ul className="hidden md:flex">{renderNavLinks()}</ul>

//       <div
//         onClick={toggleNav}
//         className="cursor-pointer pr-4 md:hidden z-10 text-gray-500"
//       >
//         {nav ? <FaTimes size={30} /> : <FaBars size={30} />}
//       </div>

//       {nav && (
//         <ul className="flex flex-col justify-center items-center absolute top-0 left-0 w-full h-screen bg-gradient-to-b from-black to-gray-800 text-gray-500">
//           {renderNavLinks()}
//         </ul>
//       )}
//     </div>
//   );
// };

// export default NavBar;
